import copy
from typing import *

from bag.layout.core import BBox
from bag.layout.routing import TrackID, WireArray
from bag.layout.template import TemplateBase

from sal.routing_grid import RoutingGridHelper, EdgeMode
from sal.via import ViaDirection

from opamp_two_stage_top_gen.layout import opamp_two_stage_top
from dmux_2to1_gen.layout import dmux_2to1
from buffer_gen.layout import buffer
from cap_mom_array_gen.layout import cap_mom_array

from .params import opamp_two_stage_fullchip_layout_params


class layout(TemplateBase):
    """Full-Chip Layout including Top Amplifier, DMUXs, Buffers, and Load Capacitors.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='opamp_two_stage_fullchip_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: opamp_two_stage_fullchip_layout_params = self.params['params'].copy()

        # Force parameters
        params.opamp_params.show_pins = True

        # create layout masters for subcells we will add later
        opamp_master = self.new_template(temp_cls=opamp_two_stage_top, params=dict(params=params.opamp_params))
        dmux_2to1_master = self.new_template(temp_cls=dmux_2to1, params=dict(params=params.dmux_2to1_params))
        buffer_master = self.new_template(temp_cls=buffer, params=dict(params=params.buffer_params))
        cap_master = self.new_template(temp_cls=cap_mom_array, params=dict(params=params.cap_params))

        # set grid, pitches and layers
        hm_layer = 4
        vm_layer = hm_layer + 1
        res = self.grid.resolution

        pitch_hm = self.grid.get_track_pitch(hm_layer, unit_mode=True)
        pitch_vm = self.grid.get_track_pitch(vm_layer, unit_mode=True)
        min_width_layer6 = self.grid.get_track_width(vm_layer+1, 1, unit_mode=True)
        min_width_required_for_M4M5_via = self.grid.get_min_track_width(vm_layer, top_w=min_width_layer6, unit_mode=True)

        # Get Subcell's width
        x_amp = opamp_master.bound_box.right_unit
        x_CL = cap_master.bound_box.right_unit
        x_Buff = buffer_master.bound_box.right_unit
        x_DMUX_2to1 = dmux_2to1_master.bound_box.right_unit

        x_col_0 = 770 * pitch_vm  # Due to TEST_Buffer, x of top layout is not set to 0
        x_space = 5 * pitch_vm  # Horizontal spacing between Subcells
        # Set center of Top layout
        x_zero = x_col_0 + max(x_DMUX_2to1 + x_Buff + 1.5 * x_space, x_amp // 2 + x_CL + 5 * x_space)
        # Set Column width on top based on subcells in each column
        x_col_1 = x_DMUX_2to1 + 1.5 * x_space
        x_col_2 = x_amp // 2 + x_space
        col_list = [x_zero, x_col_1, x_col_2]

        # if Columns are not on grid, Modify that (make it multiply of pitches)
        for col in col_list:
            if col % pitch_vm != 0:
                col = ((col // pitch_vm) + 1) * pitch_vm

        # Get Subcell's Height
        y_amp = opamp_master.bound_box.top_unit
        y_CL = cap_master.bound_box.top_unit
        y_space = 40 * pitch_hm  # Vertical spacing between Subcells

        # Set rows bottom coordinate
        y_row_0 = 80 * pitch_vm
        y_row_1 = y_row_0 + max(y_amp, y_CL) + y_space
        row_list = [y_row_0, y_row_1]

        # if rows are not on grid, Modify that (make it multiply of pitches)
        for row in row_list:
            if row % pitch_hm != 0:
                row = ((row // pitch_hm) + 1) * pitch_hm

        # add subcell instances
        CL_inst_r = self.add_instance(cap_master, 'CLN', loc=(col_list[0] + col_list[2] + 20 * pitch_vm, row_list[0]),
                                      unit_mode=True)
        CL_inst_l = self.add_instance(cap_master, 'CLP', loc=(col_list[0] - col_list[2] - 20 * pitch_vm, row_list[0]),
                                      unit_mode=True, orient='MY')

        Amp_inst = self.add_instance(opamp_master, 'AMP_Core', loc=(col_list[0] - col_list[2] + x_space, row_list[0]),
                                     unit_mode=True)  # to do

        grid_helper = RoutingGridHelper(template_base=self,
                                        unit_mode=True,
                                        layer_id=vm_layer,  # not used here
                                        track_width=1)  # not used here

        grid_helper.add_power_ring(Amp_inst.bound_box, pitch_vm, extend_to_bottom=32)  # Add power ring to Top amplifier
        Buffer_inst_p = self.add_instance(buffer_master, 'Buff_p', loc=(col_list[0] + col_list[1], row_list[1]),
                                          unit_mode=True)
        Buffer_inst_n = self.add_instance(buffer_master, 'Buff_n', loc=(col_list[0] - col_list[1], row_list[1]),
                                          unit_mode=True, orient='MY')
        TEST_Buffer_inst = self.add_instance(buffer_master, 'TEST_Buff', loc=(80 * pitch_vm, row_list[1]),
                                             unit_mode=True)
        grid_helper.add_power_ring(TEST_Buffer_inst.bound_box, pitch_vm,extend_to_top=32)  # Add power ring to TEST Buffer
        DMUX_inst_r = self.add_instance(dmux_2to1_master, 'DMUXP', loc=(col_list[0] + x_space // 2, row_list[1]),
                                        unit_mode=True)
        DMUX_inst_l = self.add_instance(dmux_2to1_master, 'DMUXN', loc=(col_list[0] - x_space // 2, row_list[1]),
                                        unit_mode=True,
                                        orient='MY')

        # get input/output wires from Subcells
        Amp_outn_warr = Amp_inst.get_all_port_pins('outn')[0]
        Amp_outp_warr = Amp_inst.get_all_port_pins('outp')[0]
        Amp_VSS_warrs, Amp_VSS_warrs1, Amp_VSS_warrs2 = Amp_inst.get_all_port_pins('VSS')[0:3]

        CL_r_plus_warr = CL_inst_r.get_all_port_pins('plus')[0]
        CL_r_minus_warrs = CL_inst_r.get_all_port_pins('minus')[0]
        CL_l_plus_warr = CL_inst_l.get_all_port_pins('plus')[0]
        CL_l_minus_warrs = CL_inst_l.get_all_port_pins('minus')[0]
        CL_rt_VDD_warrs = CL_inst_r.get_all_port_pins('VDD')[params.cap_params.ny]
        CL_rb_VDD_warrs = CL_inst_r.get_all_port_pins('VDD')[params.cap_params.ny + 1]
        CL_r_VDD_warrs = CL_inst_r.get_all_port_pins('VDD')[0]
        CL_lt_VDD_warrs = CL_inst_l.get_all_port_pins('VDD')[params.cap_params.ny]
        CL_lb_VDD_warrs = CL_inst_l.get_all_port_pins('VDD')[params.cap_params.ny + 1]
        CL_l_VDD_warrs = CL_inst_l.get_all_port_pins('VDD')[0]
        CL_r_VSS_warrs = CL_inst_r.get_all_port_pins('VSS')[0]
        CL_l_VSS_warrs = CL_inst_l.get_all_port_pins('VSS')[0]

        DMUX_l_OUTB_warrs = DMUX_inst_l.get_all_port_pins('OUTB')[0]
        DMUX_l_CNT_warrs = DMUX_inst_l.get_all_port_pins('CNT')[0]
        DMUX_l_ENB_warrs = DMUX_inst_l.get_all_port_pins('ENB')[0]
        DMUX_l_EN_warrs = DMUX_inst_l.get_all_port_pins('EN')[0]
        DMUX_l_IN_warrs = DMUX_inst_l.get_all_port_pins('IN')[0]
        DMUX_l_VDD_warrs = DMUX_inst_l.get_all_port_pins('VDD')[1]
        DMUX_l_VSS_warrs = DMUX_inst_l.get_all_port_pins('VSS')[0]

        DMUX_r_OUTB_warrs = DMUX_inst_r.get_all_port_pins('OUTB')[0]
        DMUX_r_CNT_warrs = DMUX_inst_r.get_all_port_pins('CNT')[0]
        DMUX_r_ENB_warrs = DMUX_inst_r.get_all_port_pins('ENB')[0]
        DMUX_r_EN_warrs = DMUX_inst_r.get_all_port_pins('EN')[0]
        DMUX_r_IN_warrs = DMUX_inst_r.get_all_port_pins('IN')[0]
        DMUX_r_VDD_warrs = DMUX_inst_r.get_all_port_pins('VDD')[1]
        DMUX_r_VSS_warrs = DMUX_inst_r.get_all_port_pins('VSS')[0]

        Buff_n_in_warrs = Buffer_inst_n.get_all_port_pins('in')[0]
        Buff_n_in1_warrs = Buffer_inst_n.get_all_port_pins('in')[1]
        Buff_n_EN_warrs = Buffer_inst_n.get_all_port_pins('EN')[0]
        Buff_n_ENB_warrs = Buffer_inst_n.get_all_port_pins('ENB')[0]
        Buff_n_VDD1_warrs = Buffer_inst_n.get_all_port_pins('VDD')[1]
        Buff_n_VDD_warrs = Buffer_inst_n.get_all_port_pins('VDD')[0]
        Buff_n_VDD2_warrs = Buffer_inst_n.get_all_port_pins('VDD')[2]
        Buff_n_VSS_warrs = Buffer_inst_n.get_all_port_pins('VSS')[0]
        Buff_n_VSS1_warrs = Buffer_inst_n.get_all_port_pins('VSS')[1]

        Buff_p_in_warrs = Buffer_inst_p.get_all_port_pins('in')[0]
        Buff_p_in1_warrs = Buffer_inst_p.get_all_port_pins('in')[1]
        Buff_p_EN_warrs = Buffer_inst_p.get_all_port_pins('EN')[0]
        Buff_p_ENB_warrs = Buffer_inst_p.get_all_port_pins('ENB')[0]
        Buff_p_VDD1_warrs = Buffer_inst_p.get_all_port_pins('VDD')[1]
        Buff_p_VDD_warrs = Buffer_inst_p.get_all_port_pins('VDD')[0]
        Buff_p_VDD2_warrs = Buffer_inst_p.get_all_port_pins('VDD')[2]
        Buff_p_VSS_warrs = Buffer_inst_p.get_all_port_pins('VSS')[0]
        Buff_p_VSS1_warrs = Buffer_inst_p.get_all_port_pins('VSS')[1]

        TEST_Buffer_EN_warrs = TEST_Buffer_inst.get_all_port_pins('EN')[0]
        TEST_Buffer_ENB_warrs = TEST_Buffer_inst.get_all_port_pins('ENB')[0]
        TEST_Buffer_VDD_warrs = TEST_Buffer_inst.get_all_port_pins('VDD')[0]
        TEST_Buffer_VDD1_warrs = TEST_Buffer_inst.get_all_port_pins('VDD')[1]
        TEST_Buffer_VDD2_warrs = TEST_Buffer_inst.get_all_port_pins('VDD')[2]
        TEST_Buffer_VSS_warrs = TEST_Buffer_inst.get_all_port_pins('VSS')[0]
        TEST_Buffer_VSS1_warrs = TEST_Buffer_inst.get_all_port_pins('VSS')[1]

        # Connect Buffers

        warr_EN_DM = self.connect_wires([DMUX_r_EN_warrs, DMUX_l_EN_warrs])
        warr_ENB_DM = self.connect_wires([DMUX_r_ENB_warrs, DMUX_l_ENB_warrs])

        # get vertical TrackIDs
        vm_w = self.grid.get_min_track_width(vm_layer, unit_mode=True)

        # Connect "Amp_outn" to next metal, adjust the coordinate of tracks
        lay_id = Amp_outn_warr.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_outn_warr.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_outn_warr.layer_id + 1, Amp_outn_warr.upper + 0.5, half_track=True)
        Amp_outn_warr1 = self.connect_to_tracks(Amp_outn_warr, TrackID(lay_id, mid_tr),
                                                track_lower=mid_coord - min_len / 2,
                                                track_upper=mid_coord + min_len / 2)

        # Connect "Amp_outp" to next metal, adjust the coordinate of tracks
        lay_id = Amp_outp_warr.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_outp_warr.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_outp_warr.layer_id + 1, Amp_outp_warr.lower - 0.5, half_track=True)
        Amp_outp_warr1 = self.connect_to_tracks(Amp_outp_warr, TrackID(lay_id, mid_tr),
                                                track_lower=mid_coord - min_len / 2,
                                                track_upper=mid_coord + min_len / 2)
        # Connect "CL_lt_VDD" to next metal, adjust the coordinate of tracks
        lay_id = CL_lt_VDD_warrs.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, CL_lt_VDD_warrs.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(CL_lt_VDD_warrs.layer_id + 1, CL_lt_VDD_warrs.upper, half_track=True)
        CL_lt_VDD_warrs1 = self.connect_to_tracks(CL_lt_VDD_warrs, TrackID(lay_id, mid_tr),
                                                  track_lower=mid_coord - min_len / 2,
                                                  track_upper=mid_coord + min_len / 2)
        # Connect "CL_rt_VDD" to next metal, adjust the coordinate of tracks
        lay_id = CL_rt_VDD_warrs.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, CL_rt_VDD_warrs.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(CL_rt_VDD_warrs.layer_id + 1, CL_rt_VDD_warrs.lower, half_track=True)
        CL_rt_VDD_warrs1 = self.connect_to_tracks(CL_rt_VDD_warrs, TrackID(lay_id, mid_tr),
                                                  track_lower=mid_coord - min_len / 2,
                                                  track_upper=mid_coord + min_len / 2)

        # Set track-ids for vertical line in empty space between columns, using metal 5 on right side
        tid_x_col_r1 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - x_space,
                                                                unit_mode=True) + 1,
                               width=2 * vm_w)
        tid_x_col_r = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[2],
                                                                         unit_mode=True) + 4,
                              width=5 * vm_w)
        tid_x_col_r2 = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[2],
                                                                          unit_mode=True) - 1,
                               width=vm_w)
        tid_x_col_r3 = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[2],
                                                                          unit_mode=True) - 2,
                               width=vm_w)

        # Set track-ids for vertical line in empty space between columns, using metal 5 on left side
        tid_x_col_l1 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - x_space),
                                                                unit_mode=True) - 2,
                               width=2 * vm_w)
        tid_x_col_l = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[2]),
                                                                         unit_mode=True) - 4,
                              width=5 * vm_w)
        tid_x_col_l2 = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[2]),
                                                                          unit_mode=True) + 1,
                               width=vm_w)
        tid_x_col_l3 = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[2]),
                                                                          unit_mode=True) + 2,
                               width=vm_w)
        # Set track-ids for Horizontal line in empty space between rows, using metal 4 and 6
        tid_y_row_13 = TrackID(hm_layer, self.grid.coord_to_nearest_track(hm_layer, (row_list[1] - y_space),
                                                                           unit_mode=True) + 2,
                               width=3 * vm_w) #Reducing metal layer to 4 rather than using 6
        tid_y_row_14 = TrackID(hm_layer, self.grid.coord_to_nearest_track(hm_layer, (row_list[1] - y_space),
                                                                          unit_mode=True) + 2,
                               width=5 * vm_w)

        # get vertical TrackIDs
        EN_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, DMUX_l_EN_warrs.upper) + 1,
                         width=vm_w)
        ENB_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, DMUX_r_EN_warrs.lower),
                          width=vm_w)
        tid_Buff_p_VDD = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Buff_p_VDD1_warrs.lower) - 1,
                                 width=2 * vm_w)
        tid_Buff_n_VDD = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Buff_n_VDD1_warrs.upper) + 1,
                                 width=2 * vm_w)
        tid_DMUX_CNT = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, DMUX_l_CNT_warrs.upper),
                               width=vm_w)
        tid_outp = TrackID(vm_layer,
                           self.grid.coord_to_nearest_track(vm_layer, Amp_outp_warr.middle, half_track=True) + 2,
                           width=2 * vm_w)
        tid_outn = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Amp_outn_warr.middle, half_track=True) - 2,
                           width=2 * vm_w)
        # Connect the buffers
        warr_EN = self.connect_wires([Buff_n_EN_warrs, Buff_p_EN_warrs, TEST_Buffer_EN_warrs])
        warr_ENB = self.connect_wires([Buff_n_ENB_warrs, TEST_Buffer_ENB_warrs])
        # We need to reroute ENB signal specifically because of the shorts caused by its traces touching to 'VSS'
        # Hence we choose a track that is higher than the track containing ENB pin of left-side buffer instance
        ENB_warr_n = grid_helper.connect_level_up(Buff_n_ENB_warrs,ViaDirection.TOP, 0)
        ENB_warr_p = grid_helper.connect_level_up(Buff_p_ENB_warrs,ViaDirection.TOP, 0)
        ENB_connected_warr = self.connect_to_tracks([ENB_warr_n,ENB_warr_p], TrackID(hm_layer,Buff_n_ENB_warrs.track_id.base_index+3))
        
        # Connect Tracks using defined Track_ids
        self.connect_to_tracks([Buff_n_EN_warrs, DMUX_r_ENB_warrs], EN_tid)
        self.connect_to_tracks([ENB_connected_warr, DMUX_r_EN_warrs], ENB_tid)

        Buff_n_in = self.connect_to_tracks([DMUX_l_OUTB_warrs, Buff_n_in_warrs, Buff_n_in1_warrs], tid_Buff_n_VDD)
        Buff_p_in = self.connect_to_tracks([DMUX_r_OUTB_warrs, Buff_p_in_warrs, Buff_p_in1_warrs], tid_Buff_p_VDD)
        tid_DMUX_r_IN = self.connect_to_tracks([DMUX_r_IN_warrs], tid_x_col_r1)
        tid_DMUX_l_IN = self.connect_to_tracks([DMUX_l_IN_warrs], tid_x_col_l1)
        tid_Amp_outp = self.connect_to_tracks([Amp_outp_warr], tid_outp)
        tid_Amp_outn = self.connect_to_tracks([Amp_outn_warr], tid_outn)
        Amp_outp_DMUX = self.connect_to_tracks([tid_DMUX_r_IN, tid_Amp_outp], tid_y_row_14)
        Amp_outn_DMUX = self.connect_to_tracks([tid_DMUX_l_IN, tid_Amp_outn], tid_y_row_14)

        CL_r_minus_warr = self.connect_to_tracks([CL_r_minus_warrs], tid_x_col_r)
        Buff_p_in_CL = self.connect_to_tracks([CL_r_minus_warr, Buff_p_in], tid_y_row_13)

        CL_l_minus_warr = self.connect_to_tracks([CL_l_minus_warrs], tid_x_col_l)
        Buff_n_in_CL = self.connect_to_tracks([CL_l_minus_warr, Buff_n_in], tid_y_row_13)

        CL_l_VDD = self.connect_to_tracks([CL_lt_VDD_warrs, CL_lb_VDD_warrs, CL_l_VDD_warrs], tid_x_col_l3)
        CL_l_VSS = self.connect_to_tracks([CL_l_plus_warr, Buff_p_VSS_warrs, CL_l_VSS_warrs], tid_x_col_l2)
        CL_r_VDD = self.connect_to_tracks([CL_rt_VDD_warrs, CL_rb_VDD_warrs, CL_r_VDD_warrs], tid_x_col_r3)
        CL_r_VSS = self.connect_to_tracks(
            [CL_r_plus_warr, Buff_n_VSS_warrs, CL_r_VSS_warrs], tid_x_col_r2)

        DMUX_CNT = self.connect_wires([DMUX_l_CNT_warrs, DMUX_r_CNT_warrs])

        # Set Bounding Box and size of Top block
        tot_box = Amp_inst.bound_box.merge(Amp_inst.bound_box)
        for inst in [DMUX_inst_l, DMUX_inst_r, CL_inst_l, CL_inst_r, TEST_Buffer_inst, Buffer_inst_n, Buffer_inst_p]:
            tot_box = tot_box.merge(inst.bound_box)
        self.add_cell_boundary(tot_box)
        grid_helper.add_top_power_ring(tot_box, pitch_vm, vm_layer)
        tot_box = tot_box.extend(0, 0)
        tot_box = tot_box.extend(tot_box.right_unit + 100 * pitch_vm, tot_box.top_unit + 100 * pitch_vm, unit_mode=True)
        self.add_cell_boundary(tot_box)

        tot_box1 = Buffer_inst_n.bound_box.merge(Buffer_inst_p.bound_box)
        for inst in [DMUX_inst_l, DMUX_inst_r]:
            tot_box1 = tot_box1.merge(inst.bound_box)

        # Layer names (tech agnostic)

        vdd_layer_name = self.grid.tech_info.get_layer_name(2)
        vss_layer_name = self.grid.tech_info.get_layer_name(1)

        # Connect VSS of all Subcells to VSS ring around top layout
        box = BBox(tot_box1.left_unit - 20 * pitch_vm, tot_box1.bottom_unit - 8 * pitch_vm,
                   tot_box1.right_unit + 20 * pitch_vm, tot_box1.bottom_unit - 5 * pitch_vm,
                   res, unit_mode=True)
        grid_helper.connect_warr_to_box(Buff_n_VSS_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(Buff_p_VSS_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(Buff_n_VSS1_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(Buff_p_VSS1_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(DMUX_l_VSS_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(DMUX_r_VSS_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(TEST_Buffer_VSS_warrs, box, pitch_vm, vss_layer_name)
        grid_helper.connect_warr_to_box(TEST_Buffer_VSS1_warrs, box, pitch_vm, vss_layer_name)

        # Connect VDD of all Subcells to VDD ring around top layout
        box = BBox(tot_box1.left_unit - 20 * pitch_vm, tot_box1.top_unit + 37 * pitch_vm,
                   tot_box1.right_unit + 20 * pitch_vm, tot_box1.top_unit + 40 * pitch_vm,
                   res, unit_mode=True)
        grid_helper.connect_warr_to_box(Buff_n_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(Buff_p_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(Buff_n_VDD1_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(Buff_p_VDD1_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(Buff_n_VDD2_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(Buff_p_VDD2_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(DMUX_l_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(DMUX_r_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(TEST_Buffer_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(TEST_Buffer_VDD1_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(TEST_Buffer_VDD2_warrs, box, pitch_vm, vdd_layer_name)

        # Add power ring to Subcells
        grid_helper.add_power_ring(tot_box1, pitch_vm,extend_to_top=32)
        grid_helper.add_power_ring(CL_inst_r.bound_box, pitch_vm, extend_to_bottom=32)
        grid_helper.add_power_ring(CL_inst_l.bound_box, pitch_vm, extend_to_bottom=32)
        box = BBox(CL_inst_l.bound_box.left_unit - 20 * pitch_vm, CL_inst_l.bound_box.bottom_unit - 40 * pitch_vm,
                   CL_inst_r.bound_box.right_unit + 20 * pitch_vm, CL_inst_l.bound_box.bottom_unit - 37 * pitch_vm,
                   res, unit_mode=True)

        # Connect Load capacitor to power ring around
        grid_helper.connect_warr_to_box(CL_lt_VDD_warrs, box, pitch_vm, vdd_layer_name)
        grid_helper.connect_warr_to_box(CL_rt_VDD_warrs, box, pitch_vm, vdd_layer_name)

        # Extend Amplifier's VSS wire arrays to power ring
        box = BBox(Amp_inst.bound_box.left_unit - 20 * pitch_vm, Amp_inst.bound_box.bottom_unit - 40 * pitch_vm,
                   Amp_inst.bound_box.right_unit + 20 * pitch_vm, Amp_inst.bound_box.bottom_unit - 37 * pitch_vm,
                   res, unit_mode=True)
        grid_helper.connect_warr_to_box(Amp_VSS_warrs1, box, pitch_vm, vss_layer_name)

        # Connect "outp" to layer 6, Add Pin

        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='out',
                                    inst=Buffer_inst_p,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='output',
                                    new_name='out_p',
                                    step=3, via_dir1=ViaDirection.BOTTOM, step1=1,
                                    min_width=min_width_required_for_M4M5_via)

        # Connect "outn" to M7, Add Pin

        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='out',
                                    inst=Buffer_inst_n,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='output',
                                    new_name='out_n',
                                    step=3, via_dir1=ViaDirection.BOTTOM, step1=3,
                                    min_width=min_width_required_for_M4M5_via)
        
        # Connect "Test_Buffer_out" to M7, Add Pin

        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='out',
                                    inst=TEST_Buffer_inst,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit, top=tot_box1.right_unit,
                                    pin_type='output',
                                    new_name='TEST_Buff_out',
                                    step=0, via_dir1=ViaDirection.BOTTOM, step1=23,
                                    min_width=min_width_required_for_M4M5_via)

        # Bring Pins to the edges and Add Pins
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='ibias',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit, top=tot_box1.right_unit,
                                    pin_type='input',
                                    step=3,
                                    min_width=min_width_required_for_M4M5_via,
                                    via_dir1=ViaDirection.TOP, step1=1)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='inp',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='input',
                                    step=-10,
                                    min_width=min_width_required_for_M4M5_via,
                                    via_dir1=ViaDirection.BOTTOM, step1=0)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='inn',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit,  top=tot_box.right_unit,
                                    pin_type='input',
                                    step=-9, step1=0,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='en_Amp',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='inputOutput',
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='CNT',
                                    inst=DMUX_inst_r,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='input',
                                    new_name='CNT4',
                                    step=10, via_dir1=ViaDirection.BOTTOM, step1=30,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='OUTA',
                                    inst=DMUX_inst_r,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='output',
                                    new_name='DC_outp',
                                    step=2, via_dir1=ViaDirection.BOTTOM, step1=17,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='OUTA',
                                    inst=DMUX_inst_l,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit,
                                    top=tot_box.right_unit,
                                    pin_type='output',
                                    new_name='DC_outn',
                                    step=2, via_dir1=ViaDirection.BOTTOM, step1=18,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='in',
                                    inst=TEST_Buffer_inst,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit, top=tot_box1.right_unit,
                                    pin_type='input',
                                    new_name='TEST_Buff_in',
                                    step=0, via_dir1=ViaDirection.BOTTOM, step1=24,
                                    min_width=min_width_required_for_M4M5_via)

        # Connect en_CMFB terminal to layer 6
        layer5_en_CMFB_warr = Amp_inst.get_all_port_pins('en_CMFB')[0]
        layer6_en_CMFB_tr_idx = self.grid.coord_to_nearest_track(layer_id=vm_layer+1, 
                                                               coord=layer5_en_CMFB_warr.middle_unit,
                                                               unit_mode=True)
        layer6_en_CMFB_tr_id = TrackID(vm_layer+1, layer6_en_CMFB_tr_idx)
        self.add_via_on_grid(vm_layer, layer5_en_CMFB_warr.track_id.base_index-1, layer6_en_CMFB_tr_id.base_index, bot_width=min_width_required_for_M4M5_via)
        warr = self.add_wires(vm_layer+1, layer6_en_CMFB_tr_id.base_index, tot_box1.left_unit, tot_box.right_unit,
                              unit_mode=True, width=1)
        self.add_pin('en_CMFB', warr, 'en_CMFB', show=True, edge_mode=EdgeMode.UPPER_END)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='CNT2',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='input',
                                    step=20, via_dir1=ViaDirection.TOP, step1=2,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='CNT3',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=tot_box1.left_unit, top=tot_box.right_unit,
                                    pin_type='input',
                                    step=20, via_dir1=ViaDirection.TOP, step1=1,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='cmfb_bias',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit, top=tot_box1.right_unit,
                                    pin_type='inputOutput',
                                    step=-8, via_dir1=ViaDirection.TOP, step1=-1,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='voutcm',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=x_zero + 5 * pitch_vm,
                                    top=tot_box.right_unit,
                                    pin_type='input',
                                    step=1, via_dir1=ViaDirection.BOTTOM, step1=0,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='CNT0',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.UPPER_END,
                                    bottom=x_zero + 5 * pitch_vm, top=tot_box.right_unit,
                                    pin_type='input',
                                    step=-20, via_dir1=ViaDirection.BOTTOM, step1=0,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='CNT1',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.TOP,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit,
                                    top=x_zero - 5 * pitch_vm,
                                    pin_type='input',
                                    step=-20, via_dir1=ViaDirection.BOTTOM, step1=0,
                                    min_width=min_width_required_for_M4M5_via)
        grid_helper.extend_edge_pin(layer_id=vm_layer+1,
                                    pin_name='ref',
                                    inst=Amp_inst,
                                    via_dir=ViaDirection.BOTTOM,
                                    edge_mode=EdgeMode.LOWER_END,
                                    bottom=tot_box.left_unit,
                                    top=x_zero - 5 * pitch_vm,
                                    pin_type='input',
                                    step=2,
                                    via_dir1=ViaDirection.BOTTOM,
                                    step1=0,
                                    min_width=min_width_required_for_M4M5_via)

        # compute schematic parameters.
        self._sch_params = dict(
            opamp_sch_params=opamp_master.sch_params,
            cap_sch_params=cap_master.sch_params,
            buffer_sch_params=buffer_master.sch_params,
            dmux_2to1_sch_params=dmux_2to1_master.sch_params
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class opamp_two_stage_fullchip(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
