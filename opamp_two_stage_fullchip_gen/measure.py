from enum import Enum
import matplotlib.pyplot as plt
import numpy as np
import os
from pprint import pprint
import scipy.interpolate
import scipy.optimize
from typing import *

import bag.data
from bag.util.search import FloatBinaryIterator, BinaryIterator, minimize_cost_golden

from sal.log import info, error
from sal.testbench_base import *
from sal.testbench_params import *
from sal.simulation.measurement_base import *
from sal.simulation.simulation_mode import SimulationMode

from .params import opamp_two_stage_fullchip_measurement1_params
from .schematic import dut_wrapper_schematic
from ac_tb.testbench import testbench as ac_testbench
from ac_tb.params import ac_tb_params


class opamp_two_stage_fullchip_measurement1_state(str, MeasurementState):
    AC_SIM = 'ac_sim'
    END = 'end'

    @classmethod
    def initial_state(cls) -> MeasurementState:
        return cls.AC_SIM

    @classmethod
    def final_states(cls) -> Set[MeasurementState]:
        return {cls.END}


class opamp_two_stage_fullchip_measurement1(MeasurementBase):
    @classmethod
    def name(cls) -> str:
        return 'ac_sim'

    @classmethod
    def description(cls) -> str:
        return """
            Measurement class to collect simulation results for gain / bandwidth / phase margin / etc.
            """

    @classmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        return opamp_two_stage_fullchip_measurement1_params

    @classmethod
    def ac_testbench_setup(cls) -> TestbenchSetup:
        return TestbenchSetup(
            testbench_name='ac_tb',
            testbench_class=ac_testbench,
            dut_wrapper=DUTWrapper(
                template_cell='opamp_two_stage_fullchip_dut_wrapper',
                schematic_generator_class=dut_wrapper_schematic
            )
        )

    @classmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        return [
            cls.ac_testbench_setup()
        ]

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return opamp_two_stage_fullchip_measurement1_state

    def __init__(self, params: opamp_two_stage_fullchip_measurement1_params):
        self.params = params

    def prepare_testbench(self,
                          current_state: opamp_two_stage_fullchip_measurement1_state,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        setup = self.ac_testbench_setup()
        tb_params: ac_tb_params = self.params.testbench_params_by_name['ac_tb'].copy()

        # NOTE: we don't use cfb/rfb here, so no need to alter the params
        #       but commented code below shows how sweeping could be done

        # if current_state == opamp_two_stage_fullchip_measurement1_state.FIND_CFB:
        #     cfb_list = np.linspace(self.params.cfb * self.params.cmin_scale,
        #                            self.params.cfb * self.params.cmax_scale,
        #                            self.params.num_points).tolist()
        #
        #     tb_params.simulation_params.variables['rfb'] = self.params.rfb * (1 - self.params.res_var)
        #     tb_params.simulation_params.sweeps['cfb'] = SimulationSweepList('cfb', cfb_list)

        return TestbenchRun(setup=setup, params=tb_params)

    def process_output(self,
                       current_state: opamp_two_stage_fullchip_measurement1_state,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (opamp_two_stage_fullchip_measurement1_state,
                                                        Dict[str, Any]):
        output_list = ['vout']
        results = testbench_run.instance.get_ugb_and_pm(data=current_output, output_list=output_list)

        next_state: opamp_two_stage_fullchip_measurement1_state
        output: Dict[str, Any]

        if current_state == opamp_two_stage_fullchip_measurement1_state.AC_SIM:
            next_state = opamp_two_stage_fullchip_measurement1_state.END

            # collect the results

            # NOTE: we don't need cfb sweep in this measurement
            #       but below is how a minimum cfb could be found
            # cfb = self._find_min_cfb(self.params.phase_margin, results)

            gain_results = testbench_run.instance.get_gain_and_w3db(current_output,
                                                                    output_list,
                                                                    output_dict=results)
            corner_list = 'TT' #results['corner'].tolist() changed by F
            gain_list = gain_results['gain_vout'].tolist()     #changed by F vout --> voutp
            bw_list = gain_results['w3db_vout'].tolist()       #changed by F
            funity_list = results['funity_vout'].tolist()      #changed by F
            pm_list = results['pm_vout'].tolist()              #changed by F
            output = dict(
                corners=corner_list,
                gain=gain_list,
                bw=bw_list,
                funity=funity_list,
                pm=pm_list
            )

            return next_state, output
        else:
            raise ValueError(f"unexpected state {current_state}")

    # NOTE: below method is not used in this measurement
    #       but could be used to find the minimum capacitance
    @classmethod
    def _find_min_cfb(cls, phase_margin, results):
        axis_names = ['cfb']  # changed by F, original is ['corner', 'cfb']

        corner_list = 'TT'  # results['corner'] #changed
        corner_sort_arg = np.argsort(corner_list)  # type: Sequence[int]

        # rearrange array axis
        sweep_vars = results['sweep_params']['pm_vout']
        order = [sweep_vars.index(name) for name in axis_names]
        pm_data = np.transpose(results['pm_vout'], axes=order)

        # determine minimum cfb
        cfb_vec = results['cfb']
        cfb_idx_min = 0
        for corner_idx in corner_sort_arg:
            bin_iter = BinaryIterator(cfb_idx_min, cfb_vec.size)
            while bin_iter.has_next():
                cur_cfb_idx = bin_iter.get_next()

                pm = pm_data[cur_cfb_idx]  # changed by F, original is pm_data[corner_idx, cur_cfb_idx]
                if pm >= phase_margin:
                    bin_iter.save()
                    bin_iter.down()
                else:
                    bin_iter.up()
            cfb_idx_min = bin_iter.get_last_save()
            if cfb_idx_min is None:
                # No solution; cannot make amplifier stable
                break

        if cfb_idx_min is None:
            cfb = 9.6e-14
        #            raise ValueError('Cannot determine cfb.')
        else:
            cfb = cfb_vec[cfb_idx_min]

        return cfb

    # def post_simulation_processing(self,
    #                                testbench: TestbenchBase,
    #                                mode: SimulationMode,
    #                                hdf5_path: str):
    #     # load simulation results from save file
    #     tb_results = bag.data.load_sim_file(hdf5_path)
    #
    #     info("Testbench results parsed from HDF5:")
    #     pprint(tb_results)
    #
    #     info(f"{self.__class__.__name__}: Running post-simulation processing...")
    #     plot = True
    #
    #     if testbench.package == 'ac_tb':
    #         postprocess__ac_tb(tb_results=tb_results, plot=plot)
#
#
# def postprocess__ac_tb(tb_results: Dict,
#                        plot: bool):
#     info("AC post-processing...")
#     #result_list = split_data_by_sweep(tb_results, ['vout_tran'])
