#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_params import *
from sal.simulation.measurement_base import *
from sal.transistor import ChannelType

from buffer_gen.params import buffer_layout_params
from cap_mom_gen.params import cap_mom_layout_params
from cap_mom_array_gen.params import cap_mom_array_layout_params
from cmfb_gen.params import cmfb_layout_params
from dmux_2to1_gen.params import dmux_2to1_layout_params
from half_vdd_gen.params import half_vdd_layout_params
from inverter2_gen.params import inverter2_layout_params
from inverter_en_gen.params import inverter_en_layout_params
from mux_2to1_gen.params import mux_2to1_layout_params
from mux_sw_gen.params import mux_sw_layout_params
from nmos_switch_gen.params import nmos_switch_layout_params
from opamp_two_stage_top_gen.params import opamp_two_stage_top_layout_params
from pseudo_resistor_gen.params import pseudo_resistor_layout_params
from pseudo_resistor_ctrl_gen.params import pseudo_resistor_ctrl_layout_params
from resistor_array_gen.params import resistor_array_layout_params
from resistor_diff_gen.params import resistor_diff_layout_params
from tgate_gen.params import tgate_layout_params
from two_stage_opamp_nmos_gen.params import two_stage_opamp_nmos_layout_params

from ac_tb.testbench import testbench as ac_testbench
from ac_tb.params import ac_tb_params

from .schematic import dut_wrapper_schematic


@dataclass
class opamp_two_stage_fullchip_layout_params(LayoutParamsBase):
    """
    Parameter class for opamp_two_stage_fullchip_gen

    Args:
    ----
    opamp_params : opamp_two_stage_top_layout_params
        Parameters for opamp sub-generator

    dmux_2to1_params : dmux_2to1_layout_params
        Parameters for dmux sub-generators

    buffer_params: buffer_layout_params
        Parameters for buffer sub-generators

    cap_params: cap_mom_array_layout_params
        Parameters for mom capacitor sub-generators

    cap_load_params: cap_mom_array_layout_params
        Parameters for load capacitor sub-generators

    show_pins : bool
        True to create pin labels
    """

    opamp_params: opamp_two_stage_top_layout_params
    dmux_2to1_params: dmux_2to1_layout_params
    buffer_params: buffer_layout_params
    cap_params: cap_mom_array_layout_params
    cap_load_params: cap_mom_array_layout_params
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> opamp_two_stage_fullchip_layout_params:
        # TODO: explicit set of parameters for the top-level for finfet
        #       (analog to planar_defaults())
        return opamp_two_stage_fullchip_layout_params(
            opamp_params=opamp_two_stage_top_layout_params.finfet_defaults(min_lch),
            dmux_2to1_params=dmux_2to1_layout_params.finfet_defaults(min_lch),
            buffer_params=buffer_layout_params.finfet_defaults(min_lch),
            cap_params=cap_mom_array_layout_params.finfet_defaults(min_lch),
            cap_load_params=cap_mom_array_layout_params.finfet_defaults(min_lch),
            show_pins=True,
        )
    ## TIP: It is always better to load the default params for every child generator and then modify specific parameters, 
    ## instead of copying and pasting all of them again. By following this tip, it would be much more easier to see which 
    ## of the parameters are actually different from the default ones.
    @classmethod
    def planar_defaults(cls, min_lch: float) -> opamp_two_stage_fullchip_layout_params:
        lch = min_lch * 3
        ntap_w_default = 10 * min_lch
        ptap_w_default = 10 * min_lch

        return opamp_two_stage_fullchip_layout_params(
            opamp_params=opamp_two_stage_top_layout_params(
                opamp_params=two_stage_opamp_nmos_layout_params(
                    ntap_w=ntap_w_default,
                    ptap_w=ptap_w_default,
                    lch=lch,
                    lch_dict={
                        'diode': min_lch,
                        'in': min_lch,
                        'ngm': min_lch,
                        'tail': min_lch,
                        'load': min_lch
                    },
                    th_dict={
                        'Dummy_NB': 'lvt',
                        'tail_B': 'lvt',
                        'tail_T': 'lvt',
                        'In_B': 'lvt',
                        'In_T': 'lvt',
                        'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt',
                        'load_B': 'lvt',
                        'load_T': 'lvt',
                        'Dummy_PT': 'lvt',
                    },
                    fg_dum=4,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    top_layer=5,
                    w_dict={
                        'Dummy_NB': 10 * min_lch,
                        'tail_B': 10 * min_lch,
                        'tail_T': 10 * min_lch,
                        'In_B': 10 * min_lch,
                        'In_T': 10 * min_lch,
                        'Dummy_NT': 10 * min_lch,
                        'Dummy_PB': 10 * min_lch,
                        'load_B': 10 * min_lch,
                        'load_T': 10 * min_lch,
                        'Dummy_PT': 10 * min_lch,
                    },
                    stack_dict={
                        'diode': 1,
                        'in': 1,
                        'ngm': 1,
                        'tail': 1,
                    },
                    seg_dict={
                        'diode1': 8,
                        'diode2': 12,
                        'in': 24,
                        'ngm1': 6,
                        'ngm2': 12,
                        'ref': 4,
                        'res': 4,
                        'tail1': 40,
                        'tail2': 6,
                        'tailcm': 2,
                        'vbb': 4,
                        'vb2b': 4,
                        'vbm': 4,
                        'vb2m': 8,
                        'vbt': 2,
                        'vb2t': 2,
                        'cmfb': 2,
                        'comp': 16,
                        'en': 6,
                        'Invn': 2,
                        'Invp': 6,
                    },
                    guard_ring_nf=2,
                    show_pins=True,
                ),
                cmfb_params=cmfb_layout_params(
                    ntap_w=ntap_w_default,
                    ptap_w=ptap_w_default,
                    lch=lch,
                    th_dict={
                        'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                        'tail_T': 'lvt', 'tail_B': 'lvt',
                        'In': 'lvt', 'load': 'lvt'
                    },
                    fg_dum=4,
                    top_layer=5,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    ndum_mid_stages=2,
                    w_dict={
                        'Dummy_NB': 20 * min_lch, 'Dummy_NT': 20 * min_lch,
                        'Dummy_PB': 20 * min_lch, 'Dummy_PT': 20 * min_lch,
                        'tail_T': 20 * min_lch, 'tail_B': 20 * min_lch,
                        'In': 20 * min_lch, 'load': 20 * min_lch
                    },
                    seg_dict={'cs': 8, 'cs_diod': 4, 'amp': 2, 'load': 2, 'sw_en': 4, 'en_inv': 4},
                    guard_ring_nf=2,
                    show_pins=True,
                ),
                mux_2to1_params=mux_2to1_layout_params(
                    inverter_params=inverter2_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=4,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        ndum_mid_stages=2,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 20, 'p': 40},
                        guard_ring_nf=2,
                        show_pins=True,
                        out_conn='g',
                    ),
                    tgate_params=tgate_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=4,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        ndum_mid_stages=2,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 20, 'p': 40},
                        guard_ring_nf=2,
                        show_pins=True,
                        out_conn='g',
                    ),
                    show_pins=True,
                ),
                mux_sw_params=mux_sw_layout_params(
                    inverter_params=inverter2_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=4,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        ndum_mid_stages=2,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 20, 'p': 40},
                        guard_ring_nf=2,
                        show_pins=True,
                        out_conn='g',
                    ),
                    nmos_sw_params=nmos_switch_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=2,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        w_dict={
                            'Dummy_NB': 17 * min_lch, 'Dummy_NT': 17 * min_lch,
                            'Dummy_PB': 17 * min_lch, 'Dummy_PT': 17 * min_lch,
                            'n': 17 * min_lch, 'p': 17 * min_lch
                        },
                        seg_dict={'n': 2, 'p': 6},
                        guard_ring_nf=2,
                        show_pins=True,
                    ),
                    show_pins=True,
                    guard_ring_nf=1,
                ),
                dres_params=resistor_diff_layout_params(
                    l=50 * min_lch,
                    w=25 * min_lch,
                    io_width_ntr=1,
                    sub_type='ntap',
                    threshold='standard',
                    nx=8,
                    ny=2,
                    res_type='standard',
                    em_specs={},
                    delete_vdd_pin=False,
                    top_layer=5,
                    res_conn='p',
                    ndum=1,
                ),
                pres_params=pseudo_resistor_ctrl_layout_params(
                    inverter_params=inverter2_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=4,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        ndum_mid_stages=2,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 20, 'p': 40},
                        guard_ring_nf=2,
                        show_pins=True,
                        out_conn='g',
                    ),
                    pseudo_resistor_params=pseudo_resistor_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=2,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 10, 'p': 3},
                        guard_ring_nf=2,
                        show_pins=True,
                    ),
                    show_pins=True
                ),
                hvdd_params=half_vdd_layout_params(
                    inverter_params=inverter2_layout_params(
                        ntap_w=ntap_w_default,
                        ptap_w=ptap_w_default,
                        lch=lch,
                        th_dict={
                            'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                            'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                            'n': 'lvt', 'p': 'lvt'
                        },
                        ndum=4,
                        tr_spaces={},
                        tr_widths={'sig': {4: 1}},
                        top_layer=5,
                        ndum_mid_stages=2,
                        w_dict={
                            'Dummy_NB': 10 * min_lch, 'Dummy_NT': 10 * min_lch,
                            'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                            'n': 10 * min_lch, 'p': 10 * min_lch
                        },
                        seg_dict={'n': 20, 'p': 40},
                        guard_ring_nf=2,
                        show_pins=True,
                        out_conn='g',
                    ),
                    res_array_params=resistor_array_layout_params(
                        l=50 * min_lch,
                        w=25 * min_lch,
                        sub_lch=min_lch,
                        sub_w=10 * min_lch,
                        sub_type='ntap',
                        threshold='standard',
                        nx=4,  # 2
                        ny=5,  # 5
                        res_type='standard',
                        grid_type='standard',
                        em_specs={},
                        ext_dir='',
                        show_pins=True,
                        top_layer=5,
                        add_dnw_ring=False,
                        res_conn='p',
                        ndum=1,
                        port_width=4,
                    ),
                    guard_ring_nf=2,
                    show_pins=True
                ),
                cap_params=cap_mom_array_layout_params(
                    cap_params=cap_mom_layout_params(
                        bot_layer=3,
                        top_layer=5,
                        width=512*5,  # 576, #550,
                        height=480*5,
                        margin=0,
                        in_tid=None,
                        out_tid=None,
                        port_tr_w=1,
                        options=None,
                        fill_config=None,
                        fill_dummy=False,
                        fill_pitch=2,
                        mos_type=ChannelType.N,
                        threshold='standard',
                        half_blk_x=True,
                        half_blk_y=True,
                        sub_name="VSS",
                        show_pins=True,
                    ),
                    nx=8,
                    ny=4,
                    ndum=1,
                    sub_lch=3 * min_lch,
                    sub_w=17 * min_lch,  # NOTE: 5.0e-7 would be 16.66666
                    sub_type='ntap',
                    sub_threshold='standard',
                    sub_tr_w=4,
                    show_pins=True,
                ),
                show_pins=True,
            ),
            dmux_2to1_params=dmux_2to1_layout_params(
                inverter_params=inverter2_layout_params(
                    ntap_w=ntap_w_default,
                    ptap_w=ptap_w_default,
                    lch=lch,
                    th_dict={
                        'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                        'n': 'lvt', 'p': 'lvt'
                    },
                    ndum=4,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    top_layer=5,
                    ndum_mid_stages=2,
                    w_dict={
                        'Dummy_NB': 30 * min_lch,
                        'Dummy_NT': 10 * min_lch,
                        'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                        'n': 10 * min_lch, 'p': 10 * min_lch
                    },
                    seg_dict={'n': 2, 'p': 6},
                    guard_ring_nf=2,
                    show_pins=True,
                    out_conn='g',
                ),
                tgate_params=tgate_layout_params(
                    ntap_w=ntap_w_default,
                    ptap_w=ptap_w_default,
                    lch=lch,
                    th_dict={
                        'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                        'n': 'lvt', 'p': 'lvt'
                    },
                    ndum=4,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    top_layer=5,
                    ndum_mid_stages=2,
                    w_dict={
                        'Dummy_NB': 30 * min_lch, 'Dummy_NT': 10 * min_lch,
                        'Dummy_PB': 10 * min_lch, 'Dummy_PT': 10 * min_lch,
                        'n': 10 * min_lch, 'p': 10 * min_lch
                    },
                    seg_dict={'n': 20, 'p': 60},
                    guard_ring_nf=2,
                    show_pins=True,
                    out_conn='g',
                ),
                show_pins=True,
            ),
            buffer_params=buffer_layout_params(
                inverter1_params=inverter_en_layout_params(
                    ntap_w=17 * min_lch,
                    ptap_w=17 * min_lch,
                    lch=lch,
                    th_dict={
                        'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                        'n': 'lvt', 'p': 'lvt'
                    },
                    ndum=6,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    top_layer=5,
                    ndum_mid_stages=2,
                    w_dict={
                        'Dummy_NB': 17 * min_lch, 'Dummy_NT': 17 * min_lch,
                        'Dummy_PB': 17 * min_lch, 'Dummy_PT': 17 * min_lch,
                        'n': 25 * min_lch, 'p': 25 * min_lch
                    },
                    seg_dict={'n': 4, 'p': 6},
                    guard_ring_nf=2,
                    show_pins=True,
                    out_conn='g',
                ),
                inverter2_params=inverter_en_layout_params(
                    ntap_w=17 * min_lch,
                    ptap_w=17 * min_lch,
                    lch=lch,
                    th_dict={
                        'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
                        'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
                        'n': 'lvt', 'p': 'lvt'
                    },
                    ndum=2,
                    tr_spaces={},
                    tr_widths={'sig': {4: 1}},
                    top_layer=5,
                    ndum_mid_stages=2,
                    w_dict={
                        'Dummy_NB': 17 * min_lch, 'Dummy_NT': 17 * min_lch,
                        'Dummy_PB': 17 * min_lch, 'Dummy_PT': 17 * min_lch,
                        'n': 25 * min_lch, 'p': 25 * min_lch
                    },
                    seg_dict={'n': 60, 'p': 180},
                    guard_ring_nf=2,
                    show_pins=True,
                    out_conn='g',
                ),
                resistor_array_params=resistor_array_layout_params(
                    l=50 * min_lch,
                    w=25 * min_lch,
                    sub_lch=min_lch,
                    sub_w=10 * min_lch,
                    sub_type='ntap',
                    threshold='standard',
                    nx=4,  # 2
                    ny=5,  # 5
                    res_type='standard',
                    grid_type='standard',
                    em_specs={},
                    ext_dir='',
                    show_pins=True,
                    top_layer=5,
                    add_dnw_ring=False,
                    res_conn='p',
                    ndum=1,
                    port_width=4
                ),
                show_pins=True
            ),
            cap_params=cap_mom_array_layout_params(
                cap_params=cap_mom_layout_params(
                    bot_layer=3,
                    top_layer=5,
                    width=512*5,  # 576, #550,
                    height=480*5,
                    margin=0,
                    in_tid=None,
                    out_tid=None,
                    port_tr_w=1,
                    options=None,
                    fill_config=None,
                    fill_dummy=False,
                    fill_pitch=2,
                    mos_type=ChannelType.N,
                    threshold='standard',
                    half_blk_x=True,
                    half_blk_y=True,
                    sub_name='VSS',
                    show_pins=True,
                ),
                nx=8,
                ny=4,
                ndum=1,
                sub_lch=3 * min_lch,
                sub_w=17 * min_lch,  # NOTE: 5.0e-7 would be 16.66666
                sub_type='ntap',
                sub_threshold='standard',
                sub_tr_w=4,
                show_pins=True,
            ),
            cap_load_params=cap_mom_array_layout_params(
                cap_params=cap_mom_layout_params(
                    bot_layer=3,
                    top_layer=5,
                    width=512*5,  # 576, #550,
                    height=480*5,
                    margin=0,
                    in_tid=None,
                    out_tid=None,
                    port_tr_w=1,
                    options=None,
                    fill_config=None,
                    fill_dummy=False,
                    fill_pitch=2,
                    mos_type=ChannelType.N,
                    threshold='standard',
                    half_blk_x=True,
                    half_blk_y=True,
                    sub_name="VSS",
                    show_pins=True,
                ),
                nx=8,
                ny=4,
                ndum=1,
                sub_lch=3 * min_lch,
                sub_w=17 * min_lch,  # NOTE: 5.0e-7 would be 16.66666
                sub_type='ntap',
                sub_threshold='standard',
                sub_tr_w=4,
                show_pins=True,
            ),
            show_pins=True,
        )


@dataclass
class opamp_two_stage_fullchip_measurement1_params(MeasurementParamsBase):
    cfb: float
    # cmin_scale: float
    # cmax_scale: float
    # num_points: int
    phase_margin: float
    res_var: float
    rfb: float

    testbench_params_by_name: Dict[str, TestbenchParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> opamp_two_stage_fullchip_measurement1_params:
        dut = DUT(lib="opamp_two_stage_fullchip_generated",
                  cell="opamp_two_stage_fullchip")

        ac_defaults = ac_tb_params(
            dut=dut,  # NOTE: Framework will inject DUT
            dut_wrapper_params={
                'cfb': 9.6e-14,
                'gain_cmfb': 200.0,
                'rfb': 7.7378261,
                'vinac': 1.0,
                'vincm': 0.5,
                'vindc': 0.0,
                'voutcm': 0.5,
            },
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    #           DUT_wrapper pin  |  TB pin or signal source pin
                    # -------------------------------------------------------
                    DUTTerminal(term_name='ibias', net_name='ibias'),
                    DUTTerminal(term_name='inac', net_name='vin'),
                    DUTTerminal(term_name='indc', net_name='incm'),
                    DUTTerminal(term_name='TEST_Buff_in', net_name='vin_t'),
                    DUTTerminal(term_name='TEST_Buff_o', net_name='vout_t'),
                    DUTTerminal(term_name='TO_ac', net_name='TO_ac'),
                    DUTTerminal(term_name='TO_dc', net_name='TO_dc'),
                    DUTTerminal(term_name='outac', net_name='vout'),
                    DUTTerminal(term_name='outdc', net_name='outdc'),
                    DUTTerminal(term_name='voutcm', net_name='outcm'),
                    DUTTerminal(term_name='en_Amp', net_name='VDD'),
                    DUTTerminal(term_name='en_CMFB', net_name='VDD'),
                    DUTTerminal(term_name='cmfb_bias', net_name='cmfb_bias'),
                    DUTTerminal(term_name='CNT0', net_name='VDD'),
                    DUTTerminal(term_name='CNT1', net_name='VDD'),
                    DUTTerminal(term_name='CNT2', net_name='VDD'),
                    DUTTerminal(term_name='CNT3', net_name='VDD'),
                    DUTTerminal(term_name='CNT4', net_name='VSS'),
                ],
                v_sources=[
                    DCSignalSource(source_name='SUP',
                                   plus_net_name='VDD',
                                   minus_net_name='VSS',
                                   bias_value='vdd',
                                   cdf_parameters={'acm': 0.0}),
                    DCSignalSource(source_name='INAC',
                                   plus_net_name='vin',
                                   minus_net_name='VSS',
                                   bias_value='0',
                                   cdf_parameters={'acm': 1.0}),
                    DCSignalSource(source_name='INAC1',
                                   plus_net_name='vin_t',
                                   minus_net_name='VSS',
                                   bias_value='0',
                                   cdf_parameters={'acm': 1.0}),
                    DCSignalSource(source_name='INCM',
                                   plus_net_name='incm',
                                   minus_net_name='VSS',
                                   bias_value='vincm',
                                   cdf_parameters={}),
                    DCSignalSource(source_name='OUTCM',
                                   plus_net_name='outcm',
                                   minus_net_name='VSS',
                                   bias_value='voutcm',
                                   cdf_parameters={}),
                ],
                i_sources=[
                    DCSignalSource(source_name='BIAS',
                                   plus_net_name='VDD',
                                   minus_net_name='ibias',
                                   bias_value='ibias',
                                   cdf_parameters={}),
                ],
                instance_cdf_parameters={}
            ),

            simulation_params=TestbenchSimulationParams(
                variables={
                    'vinac': 1.0,
                    'vincm': 0.5,
                    'vindc': 0.0,
                    'voutcm': 0.5
                },
                sweeps={},
                outputs={
                    'vout': 'getData("/vout", ?result \'ac)'
                }
            ),

            ibias=20.0e-06,
            vdd=1.0,
            fstart=1e5,
            fstop=100e9,
            fndec=10,
            cload=1.0e-12,
            use_cload=True,
        )

        return opamp_two_stage_fullchip_measurement1_params(
            cfb=9.6e-14,
            # cmin_scale=1.0,
            # cmax_scale=20.0,
            # num_points=2,
            phase_margin=10.0,
            res_var=0.2,
            rfb=7.7378261,
            testbench_params_by_name={
                'ac_tb': ac_defaults
            }
        )


@dataclass
class opamp_two_stage_fullchip_params(GeneratorParamsBase):
    layout_parameters: opamp_two_stage_fullchip_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> opamp_two_stage_fullchip_params:
        return opamp_two_stage_fullchip_params(
            layout_parameters=opamp_two_stage_fullchip_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[
                opamp_two_stage_fullchip_measurement1_params.defaults(min_lch=min_lch),
            ]
        )
