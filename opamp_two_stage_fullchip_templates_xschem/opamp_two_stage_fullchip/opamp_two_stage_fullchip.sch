v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 420 -880 420 -870 {
lab=DM_p}
N 650 -890 650 -870 {
lab=DM_n}
N 650 -810 650 -790 {
lab=VSS}
N 1010 -1260 1090 -1260 {
lab=DM_p}
N 1010 -1020 1090 -1020 {
lab=DM_n}
C {devices/ipin.sym} 140 -1280 0 0 {name=p1 lab=inp}
C {devices/ipin.sym} 140 -1260 0 0 {name=p2 lab=inn}
C {devices/ipin.sym} 140 -1240 0 0 {name=p3 lab=voutcm}
C {devices/ipin.sym} 140 -1220 0 0 {name=p4 lab=ibias}
C {devices/iopin.sym} 140 -1350 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 140 -1330 2 0 {name=p6 lab=VSS}
C {devices/iopin.sym} 140 -1160 2 0 {name=p7 lab=cmfb_bias}
C {devices/iopin.sym} 140 -1140 2 0 {name=p8 lab=en_Amp}
C {devices/iopin.sym} 140 -1120 2 0 {name=p9 lab=en_CMFB}
C {devices/opin.sym} 120 -1090 0 0 {name=p10 lab=outp}
C {devices/opin.sym} 120 -1030 0 0 {name=p11 lab=CNT0}
C {devices/opin.sym} 120 -1010 0 0 {name=p12 lab=CNT1}
C {devices/opin.sym} 120 -990 0 0 {name=p13 lab=CNT2}
C {devices/opin.sym} 120 -970 0 0 {name=p14 lab=CNT3}
C {devices/opin.sym} 120 -1070 0 0 {name=p15 lab=outn}
C {devices/lab_pin.sym} 490 -1300 1 0 {name=l19 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 490 -1120 3 0 {name=l20 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 510 -1120 3 0 {name=l21 sig_type=std_logic lab=cmfb_bias}
C {devices/lab_pin.sym} 630 -1220 2 0 {name=l24 sig_type=std_logic lab=outpi}
C {devices/lab_pin.sym} 630 -1200 2 0 {name=l25 sig_type=std_logic lab=outni}
C {devices/lab_pin.sym} 400 -1210 0 0 {name=l30 sig_type=std_logic lab=inp}
C {devices/lab_pin.sym} 400 -1190 0 0 {name=l31 sig_type=std_logic lab=inn}
C {devices/lab_pin.sym} 400 -1150 0 0 {name=l33 sig_type=std_logic lab=ibias}
C {cap_mom_array_gen/cap_mom_array_templates_xschem/cap_mom_array/cap_mom_array.sym} 360 -770 0 0 {name=CLP
spiceprefix=X
}
C {devices/lab_pin.sym} 420 -880 1 0 {name=l18 sig_type=std_logic lab=DM_p}
C {devices/lab_pin.sym} 420 -780 3 0 {name=l34 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 360 -830 0 0 {name=l35 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 480 -830 2 0 {name=l36 sig_type=std_logic lab=VSS}
C {cap_mom_array_gen/cap_mom_array_templates_xschem/cap_mom_array/cap_mom_array.sym} 590 -780 0 0 {name=CLN
spiceprefix=X
}
C {devices/lab_pin.sym} 650 -890 1 0 {name=l37 sig_type=std_logic lab=DM_n}
C {devices/lab_pin.sym} 650 -790 3 0 {name=l38 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 590 -840 0 0 {name=l39 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 710 -840 2 0 {name=l40 sig_type=std_logic lab=VSS}
C {devices/opin.sym} 120 -950 0 0 {name=p16 lab=CNT4}
C {devices/opin.sym} 120 -930 0 0 {name=p17 lab=DC_outp}
C {devices/opin.sym} 120 -910 0 0 {name=p18 lab=DC_outn}
C {devices/opin.sym} 120 -890 0 0 {name=p19 lab=TEST_Buff_out}
C {devices/opin.sym} 120 -1050 0 0 {name=p20 lab=ref}
C {devices/ipin.sym} 140 -1200 0 0 {name=p21 lab=TEST_Buff_in}
C {opamp_two_stage_top_gen/opamp_two_stage_top_templates_xschem/opamp_two_stage_top/opamp_two_stage_top.sym} 390 -1110 0 0 {name=Top_Amp
spiceprefix=X
}
C {devices/lab_pin.sym} 400 -1270 0 0 {name=l1 sig_type=std_logic lab=ref}
C {devices/lab_pin.sym} 400 -1250 0 0 {name=l2 sig_type=std_logic lab=voutcm}
C {devices/lab_pin.sym} 530 -1120 3 0 {name=l3 sig_type=std_logic lab=en_Amp}
C {devices/lab_pin.sym} 550 -1120 3 0 {name=l4 sig_type=std_logic lab=en_CMFB}
C {devices/lab_pin.sym} 510 -1300 1 0 {name=l5 sig_type=std_logic lab=CNT0}
C {devices/lab_pin.sym} 530 -1300 1 0 {name=l6 sig_type=std_logic lab=CNT1}
C {devices/lab_pin.sym} 550 -1300 1 0 {name=l7 sig_type=std_logic lab=CNT2}
C {devices/lab_pin.sym} 570 -1300 1 0 {name=l8 sig_type=std_logic lab=CNT3}
C {dmux_2to1_gen/dmux_2to1_templates_xschem/dmux_2to1/dmux_2to1.sym} 850 -1210 0 0 {name=DMUXP
spiceprefix=X
}
C {dmux_2to1_gen/dmux_2to1_templates_xschem/dmux_2to1/dmux_2to1.sym} 850 -970 0 0 {name=DMUXN
spiceprefix=X
}
C {buffer_gen/buffer_templates_xschem/buffer/buffer.sym} 1030 -1200 0 0 {name=Buff_p
spiceprefix=X
}
C {buffer_gen/buffer_templates_xschem/buffer/buffer.sym} 1030 -960 0 0 {name=Buff_n
spiceprefix=X
}
C {buffer_gen/buffer_templates_xschem/buffer/buffer.sym} 1030 -740 0 0 {name=TEST_Buff
spiceprefix=X
}
C {devices/lab_pin.sym} 920 -1330 1 0 {name=l9 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1120 -1330 1 0 {name=l10 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1120 -1090 1 0 {name=l11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 920 -1090 1 0 {name=l12 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1120 -870 1 0 {name=l13 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 920 -1230 3 0 {name=l14 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 920 -990 3 0 {name=l15 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1120 -1210 3 0 {name=l16 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1120 -970 3 0 {name=l17 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1120 -750 3 0 {name=l22 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1140 -750 3 0 {name=l23 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 1140 -970 3 0 {name=l26 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 1140 -1210 3 0 {name=l27 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 1140 -1330 1 0 {name=l28 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 1140 -1090 1 0 {name=l29 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 1140 -870 1 0 {name=l32 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 1260 -1260 2 0 {name=l41 sig_type=std_logic lab=outp}
C {devices/lab_pin.sym} 1260 -1020 2 0 {name=l42 sig_type=std_logic lab=outn}
C {devices/lab_pin.sym} 1260 -800 2 0 {name=l43 sig_type=std_logic lab=TEST_Buff_out}
C {devices/lab_pin.sym} 1090 -800 0 0 {name=l44 sig_type=std_logic lab=TEST_Buff_in}
C {devices/lab_pin.sym} 890 -1280 0 0 {name=l45 sig_type=std_logic lab=outpi}
C {devices/lab_pin.sym} 890 -1040 0 0 {name=l46 sig_type=std_logic lab=outni}
C {devices/lab_pin.sym} 950 -1340 1 0 {name=l47 sig_type=std_logic lab=CNT4}
C {devices/lab_pin.sym} 950 -1100 1 0 {name=l48 sig_type=std_logic lab=CNT4}
C {devices/lab_pin.sym} 950 -1220 3 0 {name=l49 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 970 -1220 3 0 {name=l50 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 950 -980 3 0 {name=l51 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 970 -980 3 0 {name=l52 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 1010 -1290 2 0 {name=l53 sig_type=std_logic lab=DC_outp}
C {devices/lab_pin.sym} 1010 -1050 2 0 {name=l54 sig_type=std_logic lab=DC_outn}
C {devices/lab_pin.sym} 1050 -1020 3 0 {name=l55 sig_type=std_logic lab=DM_n}
C {devices/lab_pin.sym} 1050 -1260 3 0 {name=l56 sig_type=std_logic lab=DM_p}
